// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class Gamprg_2_De_Vera_TDEditorTarget : TargetRules
{
	public Gamprg_2_De_Vera_TDEditorTarget( TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		ExtraModuleNames.AddRange( new string[] { "Gamprg_2_De_Vera_TD" } );
	}
}
