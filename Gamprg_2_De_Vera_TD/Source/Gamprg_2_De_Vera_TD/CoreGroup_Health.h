// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CoreGroup_Health.generated.h"

UCLASS()
class GAMPRG_2_DE_VERA_TD_API ACoreGroup_Health : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACoreGroup_Health();

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		class UHealthComponent* Health;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<AActor*> LifeCores;

	UFUNCTION()
		void OnOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, 
		int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
