// Fill out your copyright notice in the Description page of Project Settings.


#include "Projectile_Missle.h"
#include "Enemy.h"
#include "HealthComponent.h"
#include "Kismet/KismetSystemLibrary.h"

AProjectile_Missle::AProjectile_Missle()
{

}

void AProjectile_Missle::OnOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Super::OnOverlap(OverlappedComp, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);

	if (AEnemy* enemy = Cast<AEnemy>(OtherActor))
	{
		FVector SphereSpawnLocation = enemy->GetActorLocation();
		TArray<TEnumAsByte<EObjectTypeQuery>> traceObjectTypes;
		traceObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_Pawn));
		TArray<AActor*> enemies;
		TArray<AActor*> ignoredActors;
		UClass* seekClass = AEnemy::StaticClass();
		UKismetSystemLibrary::SphereOverlapActors(GetWorld(), SphereSpawnLocation, 5000.0f, traceObjectTypes, seekClass, ignoredActors, enemies);

		for (AActor* actor : enemies)
		{
			if (AEnemy* Enemy = Cast<AEnemy>(actor))
			{
				Enemy->Health->TakeDamage(25);
			}
		}

	}

}

