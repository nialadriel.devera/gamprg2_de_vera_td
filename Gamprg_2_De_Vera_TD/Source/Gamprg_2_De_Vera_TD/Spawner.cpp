// Fill out your copyright notice in the Description page of Project Settings.


#include "Spawner.h"
#include "Enemy.h"
#include "Engine/Engine.h"
#include "WaveData.h"
#include "HealthComponent.h"
#include "BuildManager.h"
#include "Gamprg_2_De_Vera_TDGameModeBase.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ASpawner::ASpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>("SceneComponent");
	SetRootComponent(SceneComponent);
}

class AEnemy* ASpawner::spawnEnemies(TSubclassOf<class AEnemy> EnemyTypes)
{
	FActorSpawnParameters SpawnParameters;
	//SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;

	FTransform transform = FTransform(FRotator(0), GetActorLocation());
	AEnemy* spawnedEnemies = GetWorld()->SpawnActorDeferred<AEnemy>(EnemyTypes, transform, nullptr, nullptr, ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
	//AEnemy* spawnedEnemies = GetWorld()->SpawnActor<AEnemy>(EnemyTypes, GetWorldLocation(), FRotator(0), SpawnParameters); 
	spawnedEnemies->setWaypoints(waypoints);

	spawnedEnemies->Health->ScaleHealth(1.1f);
	
	return Cast<AEnemy>(UGameplayStatics::FinishSpawningActor(spawnedEnemies, transform));
	//return spawnedEnemies;
}


// Called when the game starts or when spawned
void ASpawner::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

