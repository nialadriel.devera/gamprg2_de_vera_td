// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthComponent.h"
#include "HealthComponent.h"

// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

void UHealthComponent::TakeDamage(int32 damage)
{
	currentHealth--;
	// currentHealth-= damage;
	if (currentHealth <= 0)
	{
		onDeath.Broadcast(this);
	}
}

void UHealthComponent::ScaleHealth(float scaleDifficulty)
{
	currentHealth *= scaleDifficulty;
}

// Called every frame
void UHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

