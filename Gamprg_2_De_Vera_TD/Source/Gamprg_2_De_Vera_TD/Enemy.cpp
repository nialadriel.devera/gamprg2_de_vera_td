// Fill out your copyright notice in the Description page of Project Settings.


#include "Enemy.h"
#include "Components/SceneComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "Waypoint.h"
#include "HealthComponent.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
AEnemy::AEnemy()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>("SceneComponent");
	SetRootComponent(SceneComponent);

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("StaticMesh");
	StaticMesh->SetupAttachment(RootComponent);

	Health = CreateDefaultSubobject<UHealthComponent>("Health");
	Health->onDeath.AddDynamic(this, &AEnemy::Die);

	Movement = CreateDefaultSubobject<UFloatingPawnMovement>("Movement");

	SphereCollider = CreateDefaultSubobject<USphereComponent>("SphereCollider");
}

void AEnemy::Die(UHealthComponent* health)
{
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Enemy Dead"));
	isDead = true;
	Destroy();
}

void AEnemy::setWaypoints(TArray<AWaypoint*> waypoints)
{
	Waypoints = waypoints;
}

// Called when the game starts or when spawned
void AEnemy::BeginPlay()
{
	Super::BeginPlay();
	SpawnDefaultController();
}

void AEnemy::MoveToActor()
{
	FRotator rot = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), Waypoints[waypointsCheck]->GetActorLocation());
	SetActorRotation(rot);
	Movement->AddInputVector(GetActorForwardVector());
}

// Called every frame
void AEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (Waypoints.Num() <= waypointsCheck) return;
	MoveToActor();
	if ((GetActorLocation() - Waypoints[waypointsCheck]->GetActorLocation()).Size() <= 100)
	{
		waypointsCheck++;
	}

}

// Called to bind functionality to input
void AEnemy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

