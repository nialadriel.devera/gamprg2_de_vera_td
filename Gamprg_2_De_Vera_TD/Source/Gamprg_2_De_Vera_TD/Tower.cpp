// Fill out your copyright notice in the Description page of Project Settings.


#include "Tower.h"
#include "Components/SceneComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"
#include "Components/ArrowComponent.h"
#include "Enemy.h"
#include "Projectile.h"
#include "HealthComponent.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
ATower::ATower()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>("SceneComponent");
	SetRootComponent(SceneComponent);

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("StaticMesh");
	StaticMesh->SetupAttachment(RootComponent);
	
	SphereCollider = CreateDefaultSubobject<USphereComponent>("SphereCollider");
	SphereCollider->OnComponentBeginOverlap.AddDynamic(this, &ATower::OnOverlap);
	SphereCollider->OnComponentEndOverlap.AddDynamic(this, &ATower::OnExitOverlap);

	ArrowComponent = CreateDefaultSubobject<UArrowComponent>("ArrowComponent");
	ArrowComponent->SetupAttachment(StaticMesh);
}

void ATower::setEnemies(TArray<class AEnemy*> enemies)
{
	Enemies = enemies;
}

void ATower::RemoveEnemyInArray(UHealthComponent* Health)
{
	AEnemy* enemy = Cast<AEnemy>(Health->GetOwner());
	Enemies.Remove(enemy);
}

// Called when the game starts or when spawned
void ATower::BeginPlay()
{
	Super::BeginPlay();
	
}

void ATower::OnOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (AEnemy* enemy = Cast<AEnemy>(OtherActor))
	{
		enemiesCount++;
		Enemies.Add(enemy);
		enemy->Health->onDeath.AddDynamic(this, &ATower::RemoveEnemyInArray);
	}
}

void ATower::OnExitOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (AEnemy* enemy = Cast<AEnemy>(OtherActor))
	{
		enemiesCount--;
		Enemies.Remove(enemy);
		enemy->Health->onDeath.RemoveDynamic(this, &ATower::RemoveEnemyInArray);
	}
}

void ATower::TargetAcquisition()
{
	FRotator rot = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), Enemies[0]->GetActorLocation());
	StaticMesh->SetWorldRotation(FRotator(0, rot.Yaw - 90, 0));
}

void ATower::SpawnProjectile()
{
	FTimerHandle timerHandle;
	FTimerDelegate timerDelegate;
	FActorSpawnParameters spawnParameters;
	spawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;

	canShoot = false;
	AProjectile* spawnedProj = GetWorld()->SpawnActor<AProjectile>(BP_Projectile, ArrowComponent->GetComponentTransform(), spawnParameters);
	timerDelegate.BindLambda([this] { canShoot = true; });

	GetWorld()->GetTimerManager().SetTimer(timerHandle, timerDelegate, 1.0f, false);
}

// Called every frame
void ATower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (Enemies.Num() <= 0) return;

	if (Enemies[0] != nullptr)
	{
		TargetAcquisition();
	}

	if (canShoot)
	{
		SpawnProjectile();
	}
	
}

