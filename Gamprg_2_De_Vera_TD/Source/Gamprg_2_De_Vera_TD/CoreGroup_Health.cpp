// Fill out your copyright notice in the Description page of Project Settings.


#include "CoreGroup_Health.h"
#include "LifeCore.h"
#include "HealthComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Enemy.h"

// Sets default values
ACoreGroup_Health::ACoreGroup_Health()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Health = CreateDefaultSubobject<UHealthComponent>("Health");
}

void ACoreGroup_Health::OnOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (AEnemy* enemy = Cast<AEnemy>(OtherActor))
	{
		Health->TakeDamage(1);
		enemy->Destroy();
	}
}

// Called when the game starts or when spawned
void ACoreGroup_Health::BeginPlay()
{
	Super::BeginPlay();
	
	for (auto LifeCore : LifeCores)
	{
		ALifeCore* l = Cast<ALifeCore>(LifeCore);
		l->StaticMesh->OnComponentBeginOverlap.AddDynamic(this, &ACoreGroup_Health::OnOverlap);
	}
}

// Called every frame
void ACoreGroup_Health::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

