// Fill out your copyright notice in the Description page of Project Settings.


#include "BuildableNodes.h"

// Sets default values
ABuildableNodes::ABuildableNodes()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>("SceneComponent");
	SetRootComponent(SceneComponent);

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("StaticMesh");
	StaticMesh->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ABuildableNodes::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABuildableNodes::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

