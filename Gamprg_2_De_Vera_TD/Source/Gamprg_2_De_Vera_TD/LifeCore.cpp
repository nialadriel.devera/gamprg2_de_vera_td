// Fill out your copyright notice in the Description page of Project Settings.


#include "LifeCore.h"
#include "Components/SceneComponent.h"
#include "Components/StaticMeshComponent.h"
#include "HealthComponent.h"
#include "Enemy.h"

// Sets default values
ALifeCore::ALifeCore()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>("SceneComponent");
	SetRootComponent(SceneComponent);

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("StaticMesh");
	StaticMesh->SetupAttachment(RootComponent);

	Health = CreateDefaultSubobject<UHealthComponent>("Health");
}

// Called when the game starts or when spawned
void ALifeCore::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ALifeCore::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

