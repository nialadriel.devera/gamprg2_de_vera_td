// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Enemy.generated.h"

UCLASS()
class GAMPRG_2_DE_VERA_TD_API AEnemy : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AEnemy();

	bool isDead = false;

	UFUNCTION(BlueprintCallable)
		void Die(UHealthComponent* health);

	UFUNCTION()
		void setWaypoints(TArray<class AWaypoint*> waypoints);

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		class UHealthComponent* Health;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		class USphereComponent* SphereCollider;

	int32 waypointsCheck;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
		class USceneComponent* SceneComponent;

	UPROPERTY(VisibleAnywhere)
		class UStaticMeshComponent* StaticMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UFloatingPawnMovement* Movement;

	UFUNCTION()
		void MoveToActor();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

private:
	UPROPERTY(VisibleAnywhere)
		TArray<class AWaypoint*> Waypoints;
};
