// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Gamprg_2_De_Vera_TDGameModeBase.generated.h"


/**
 * 
 */
UCLASS()
class GAMPRG_2_DE_VERA_TD_API AGamprg_2_De_Vera_TDGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 currentWave = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class ABuildManager* buildMgr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<class ASpawner*> SpawnVariable;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<class UWaveData*> Waves;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int32 killCount;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int32 currentSpawnedUnits;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int32 maxSpawnedUnits;

	UFUNCTION(BlueprintCallable)
		void StartSpawn();

	UFUNCTION()
		void KillReward(class UHealthComponent* Health);

	UFUNCTION(BlueprintCallable)
		void waveCleared();

protected:
	virtual void BeginPlay() override;
};
