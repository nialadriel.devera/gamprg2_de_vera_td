// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Projectile.h"
#include "Projectile_Missle.generated.h"

/**
 * 
 */
UCLASS()
class GAMPRG_2_DE_VERA_TD_API AProjectile_Missle : public AProjectile
{
	GENERATED_BODY()

public:
		AProjectile_Missle();
	
protected:
	void OnOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;
};
