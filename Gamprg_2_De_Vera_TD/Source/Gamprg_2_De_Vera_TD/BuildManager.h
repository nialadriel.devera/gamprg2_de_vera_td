// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BuildManager.generated.h"

UCLASS()
class GAMPRG_2_DE_VERA_TD_API ABuildManager : public APawn
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABuildManager();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 RemainingGold;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 MaxGold;

	UFUNCTION(BlueprintPure, BlueprintCallable)
		bool CanBuyTower(int32 TowerPrice);

	UFUNCTION(BlueprintCallable)
		void SellTower(int32 TowerPrice);
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
