// Copyright Epic Games, Inc. All Rights Reserved.


#include "Gamprg_2_De_Vera_TDGameModeBase.h"
#include "Spawner.h"
#include "WaveData.h"
#include "Kismet/GameplayStatics.h"
#include "CoreGroup_Health.h"
#include "BuildManager.h"
#include "HealthComponent.h"
#include "Enemy.h"

void AGamprg_2_De_Vera_TDGameModeBase::BeginPlay()
{
	Super::BeginPlay();
}

void AGamprg_2_De_Vera_TDGameModeBase::StartSpawn()
{
	FTimerHandle timerHandle;

	maxSpawnedUnits = Waves[currentWave]->NumSpawns;

	if (currentSpawnedUnits <= maxSpawnedUnits)
	{
		GetWorldTimerManager().SetTimer(timerHandle, this, &AGamprg_2_De_Vera_TDGameModeBase::StartSpawn, 2.5f, false);
		AEnemy* spawnedEnemy = SpawnVariable[FMath::RandRange(0, 3)]->spawnEnemies(Waves[currentWave]->Monsters[0]);
		spawnedEnemy->Health->onDeath.AddDynamic(this, &AGamprg_2_De_Vera_TDGameModeBase::KillReward);
		currentSpawnedUnits++;
	}
	else if (currentSpawnedUnits >= maxSpawnedUnits)
	{
		waveCleared();
	}
}

void AGamprg_2_De_Vera_TDGameModeBase::KillReward(UHealthComponent* Health)
{
	if (currentWave <= Waves.Num()) 
	{
		buildMgr->RemainingGold += Waves[currentWave]->KillReward;
		killCount++;
	}
}

void AGamprg_2_De_Vera_TDGameModeBase::waveCleared()
{
	currentWave++;
	currentSpawnedUnits++;

	if (currentSpawnedUnits >= maxSpawnedUnits)
	{
		currentSpawnedUnits = 0;
		buildMgr->RemainingGold += Waves[currentWave]->ClearReward;
		StartSpawn();
	}
}


