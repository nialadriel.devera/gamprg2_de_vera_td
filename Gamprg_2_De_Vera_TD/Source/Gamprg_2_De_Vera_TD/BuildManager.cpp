// Fill out your copyright notice in the Description page of Project Settings.


#include "BuildManager.h"
#include "Tower.h"
#include "Enemy.h"
#include "Spawner.h"

// Sets default values
ABuildManager::ABuildManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

bool ABuildManager::CanBuyTower(int32 TowerPrice)
{
	if (RemainingGold >= TowerPrice)
	{
		RemainingGold -= TowerPrice;
		return true;
	}
	else 
	{
		return false;
	}
}

void ABuildManager::SellTower(int32 TowerPrice)
{
	int32 TotalPrice = TowerPrice /= 2;
	RemainingGold += TotalPrice;
}

// Called when the game starts or when spawned
void ABuildManager::BeginPlay()
{
	Super::BeginPlay();
	
	RemainingGold = 200;
}

// Called every frame
void ABuildManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

