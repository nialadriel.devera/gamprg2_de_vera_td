// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "LifeCore.generated.h"

UCLASS()
class GAMPRG_2_DE_VERA_TD_API ALifeCore : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ALifeCore();

	//FORCEINLINE UStaticMeshComponent* GetStaticMesh() { return StaticMesh; }

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		class UStaticMeshComponent* StaticMesh;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		class UHealthComponent* Health;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
		class USceneComponent* SceneComponent;

	UFUNCTION(BlueprintImplementableEvent)
		void OnTrigger(AEnemy* enemy);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
