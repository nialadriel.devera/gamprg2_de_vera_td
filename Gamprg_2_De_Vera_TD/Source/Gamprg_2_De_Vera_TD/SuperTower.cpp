// Fill out your copyright notice in the Description page of Project Settings.


#include "SuperTower.h"
#include "Projectile.h"
#include "Enemy.h"

// Sets default values
ASuperTower::ASuperTower()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASuperTower::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASuperTower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

