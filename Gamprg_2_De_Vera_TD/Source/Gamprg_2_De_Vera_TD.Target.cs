// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class Gamprg_2_De_Vera_TDTarget : TargetRules
{
	public Gamprg_2_De_Vera_TDTarget( TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		ExtraModuleNames.AddRange( new string[] { "Gamprg_2_De_Vera_TD" } );
	}
}
